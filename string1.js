function string1(arr) {
  let result = [];
  if (!arr) {
    return 0;
  }

  if (arr.length === 0) {
    return 0;
  }

  for (let i = 0; i <= arr.length - 1; i++) {
    let temp = arr[i];
    let tempstr = "";

    for (let j = 0; j <= arr[i].length - 1; j++) {
      if (temp[j] === "$" || temp[j] == ",") {
        continue;
      } else if (
        !Number.isInteger(parseInt(temp[j])) &&
        temp[j] !== "." &&
        temp[j] !== "-"
      ) {
        return 0;
      } else {
        tempstr += temp[j];
      }
    }
    result.push(parseFloat(tempstr));
  }
  return result;
}
module.exports = string1;
