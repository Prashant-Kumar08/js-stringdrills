function string5(str) {
  if (!str) {
    return 0;
  }
  if (typeof str !== "object") {
    return 0;
  }

  let temp = "";
  for (let i = 0; i <= str.length - 1; i++) {
    temp = temp + str[i] + " ";
  }
  return temp;
}

module.exports = string5;
