function string2(Ip) {
  if (!Ip || Ip.length === 0) {
    return 0;
  }
  if (typeof Ip !== "string" && typeof Ip !== "object") {
    return 0;
  }

  let result = [];
  let temp = "";
  for (let i = 0; i <= Ip.length - 1; i++) {
    if (!Number.isInteger(parseInt(Ip[i])) && Ip[i] !== ".") {
      return 0;
    }

    if (Ip[i] === ".") {
      result.push(parseInt(temp));
      temp = "";
      continue;
    } else {
      temp += Ip[i];
      continue;
    }
  }
  result.push(parseInt(temp));
  return result;
}

module.exports = string2;
