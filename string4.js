function string4(a) {
  if (!a) {
    return 0;
  }

  if (Object.keys(a).length === 0) {
    return 0;
  }
  if (typeof a !== "object" || a.length === 0) {
    return 0;
  }

  const fkey = a.hasOwnProperty("first_name");
  const mkey = a.hasOwnProperty("middle_name");
  const lkey = a.hasOwnProperty("last_name");

  if (!a.hasOwnProperty("first_name") || !a.hasOwnProperty("last_name")) {
    return 0;
  }

  if (mkey) {
    let fname = a.first_name.toLowerCase();
    fname = fname.charAt(0).toUpperCase() + fname.slice(1, fname.length);

    let mname = a.middle_name.toLowerCase();
    mname = mname.charAt(0).toUpperCase() + mname.slice(1, mname.length);

    let lname = a.last_name.toLowerCase();
    lname = lname.charAt(0).toUpperCase() + lname.slice(1, lname.length);

    return `${fname} ${mname} ${lname}`;
  }

  if (!mkey) {
    let fname = a.first_name.toLowerCase();
    fname = fname.charAt(0).toUpperCase() + fname.slice(1, fname.length);

    let lname = a.last_name.toLowerCase();
    lname = lname.charAt(0).toUpperCase() + lname.slice(1, lname.length);

    return `${fname}  ${lname}`;
  }
}

module.exports = string4;
